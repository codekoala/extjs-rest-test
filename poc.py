#!/usr/bin/env python

import json

from bottle import route, view, run, static_file

USERS = [{
    'id': 1,
    'username': 'User 1',
    'events': [1, 2]
}, {
    'id': 2,
    'username': 'User 2',
    'events': [3]
}]

EVENTS = [{
    'id': 1,
    'title': 'Event 1',
    'date': '2013-07-29T00:00:00',
    'user_id': 1
}, {
    'id': 2,
    'title': 'Event 2',
    'date': '2013-07-30T00:00:00',
    'user_id': 1
}, {
    'id': 3,
    'title': 'Event 3',
    'date': '2013-05-30T00:00:00',
    'user_id': 2
}]


@route('/')
@view('index')
def index():
    return {}


@route('/api/user')
def user_list():
    return json.dumps({
        'meta': {
            'limit': 100,
            'next': None,
            'offset': 0,
            'previous': None,
            'total_count': len(USERS)
        },
        'objects': USERS
    })


@route('/api/user/<pk:int>')
def user_resource(pk):
    return json.dumps({'objects': [USERS[pk - 1]]})


@route('/api/event')
def event_list():
    return json.dumps({
        'meta': {
            'limit': 100,
            'next': None,
            'offset': 0,
            'previous': None,
            'total_count': len(EVENTS)
        },
        'objects': EVENTS
    })


@route('/api/event/<pk:int>')
def event_resource(pk):
    return json.dumps({'objects': [EVENTS[pk - 1]]})


@route('/<root>/<path:path>')
def app_media(root, path):
    return static_file(path, root='./{}/'.format(root))


if __name__ == '__main__':
    run(host='localhost', port=9080, debug=True, reloader=True)
