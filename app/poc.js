Ext.application({
  name: 'POC',
  appFolder: 'app',

  models: [
    'User',
    'Event'
  ],

  stores: [
    'User',
    'Event'
  ],

  launch: function() {
    this.users = Ext.getStore('User');
    this.events = Ext.getStore('Event');

    // force it!
    this.events.load({
      callback: function (rec, op, success) {
        var ev = this.events.getById(3),
            pk = ev.get('id');

        console.log('I have an event (' + pk + '):', ev.get('title'));
        console.log('Event ' + pk + ' user ID:', ev.get('user_id'));

        ev.getUser({
          success: function (u, op) {
            console.log('Event ' + pk + ' username (should be "User 2"): ', u.get('username'));
          },
          failure: function (a, b) {
            console.log('A', a, 'B', b);
          }
        });
      },
      scope: this
    });

    this.users.load({
      callback: function(rec, op, success) {
        console.log('User 1 events (should be 2):', this.users.getById(1).events().data.items.length);
        console.log('User 2 events (should be 1):', this.users.getById(2).events().data.items.length);
      },
      scope: this
    });
  }
});
