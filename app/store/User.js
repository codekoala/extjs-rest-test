Ext.define('POC.store.User', {
  extend: 'Ext.data.Store',
  model: 'POC.model.User',
  autoLoad: true
});
