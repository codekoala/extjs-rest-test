Ext.define('POC.model.Event', {
  extend: 'Ext.data.Model',
  requires: [
    'POC.model.User',
  ],

  belongsTo: [{
    name           : 'user',
    instanceName   : 'user',
    model          : 'POC.model.User',
    associatedName : 'POC.model.User',
    getterName     : 'getUser',
    setterName     : 'setUser',
    primaryKey     : 'id',
    foreignKey     : 'user_id'
  }],

  fields: [
    {name: 'id'      , type: 'int'}      ,
    {name: 'title'   , type: 'string'}   ,
    {name: 'date'    , type: 'datetime'} ,
    {name: 'user_id' , type: 'int'}
  ],

  proxy: {
    type: 'rest',
    url: '/api/event',
    reader: {
      type: 'json',
      root: 'objects',
      totalProperty: 'total_count'
    }
  }
});
