Ext.define('POC.model.User', {
  extend: 'Ext.data.Model',

  hasMany: [{
    model: 'POC.model.Event',
    name: 'events',
    getterName: 'getEvents',
    setterName: 'setEvents',
    associationKey: 'events',
    foreignKey: 'user_id',
    autoLoad: true
  }],

  fields: [
    {name: 'id'       , type: 'int'}    ,
    {name: 'username' , type: 'string'}
  ],

  proxy: {
    type: 'rest',
    url: '/api/user',
    reader: {
      type: 'json',
      root: 'objects',
      totalProperty: 'total_count'
    }
  }
});
